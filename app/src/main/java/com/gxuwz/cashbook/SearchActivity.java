package com.gxuwz.cashbook;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gxuwz.cashbook.adapter.ListViewCompat;
import com.gxuwz.cashbook.adapter.PrivateListingAdapter;
import com.gxuwz.cashbook.bean.CostBean;
import com.gxuwz.cashbook.util.IntentUtils;

import org.litepal.crud.DataSupport;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by h on 2017/4/30.
 */

public class SearchActivity extends BaseActivity{

    @BindView(R.id.backBtn)
    ImageButton ibBack;     // 和增加页面和饼图页面的不同
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.lv_search)
    ListViewCompat lvSearch ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);     // 绑定控件到当前视图
        showCostData(DataSupport.findAll(CostBean.class));
    }

    /**
     * 按钮点击事件
     * @param v
     */
    @OnClick({R.id.tv_search, R.id.backBtn})
    public void viewOnClick(View v) {
        switch (v.getId()) {
            case R.id.tv_search:
                showDatePickDialog();
                break;
            case R.id.backBtn:
                IntentUtils.jump(SearchActivity.this,MainActivity.class);  // 返回到 MainActivity
                break;
        }
    }

    /**
     * 收支数据每一条数据的点击事件,跳转到更新页面
     * @param position
     */
    @OnItemClick(R.id.lv_search)
    public void onItemClick(int position){
        Intent intent = new Intent(SearchActivity.this, UpdateActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("costBean", lvSearch.costBean); //序列化一个对象
        intent.putExtras(bundle); //把该对象传到另一个页面
        startActivity(intent);
    }

    /**
     * 显示消费数据
     * @param costBeanList
     */
    public void showCostData(List<CostBean> costBeanList){
        PrivateListingAdapter mAdapter = new PrivateListingAdapter(this, costBeanList);
        lvSearch.setAdapter(mAdapter);
    }

    /**
     * 显示日期选择器
     */
    public void showDatePickDialog() {
        Calendar c = Calendar.getInstance();
        new DatePickerDialog(SearchActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker dp, int year, int mounth, int day) {
                        String date = year + "-" + (mounth+1) + "-" + day;
                        tvSearch.setText(date);
                        showCostData(DataSupport.where("createDate like ?", "%" + date + "%").find(CostBean.class));
                    }
                },
                c.get(Calendar.YEAR),
                c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH)).show();
    }
}
