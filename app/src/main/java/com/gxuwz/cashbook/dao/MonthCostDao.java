package com.gxuwz.cashbook.dao;

import com.gxuwz.cashbook.bean.CategoryBean;
import com.gxuwz.cashbook.bean.CostBean;
import com.gxuwz.cashbook.bean.MonthCostVO;
import com.gxuwz.cashbook.util.DateUtils;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by h on 2017/5/26.
 */

public class MonthCostDao {

    public static List<MonthCostVO> getMonthExpense(String date){
        List<CostBean> costBeanList = DataSupport.where("createDate like ? and type = ?","%" + date + "%","支出").find(CostBean.class);
        List<CategoryBean> categoryBeanList = DataSupport.where("type = ?","支出").find(CategoryBean.class);
        List<MonthCostVO> monthCostVOList = new ArrayList<MonthCostVO>();
        MonthCostVO  monthCostVO = null;
        int sum ;
        // 计算每一类的总金额
        for(int j=0;j<categoryBeanList.size();j++){
            sum = 0;
            monthCostVO = new MonthCostVO();
            for(int i=0;i<costBeanList.size();i++){
                if(costBeanList.get(i).getClazz().equals(categoryBeanList.get(j).getClazz())){
                    sum += costBeanList.get(i).getMoney();
                }
            }
            monthCostVO.setName(categoryBeanList.get(j).getClazz());
            monthCostVO.setValue(sum);
            monthCostVOList.add(monthCostVO);
        }
        return monthCostVOList;
    }

}
