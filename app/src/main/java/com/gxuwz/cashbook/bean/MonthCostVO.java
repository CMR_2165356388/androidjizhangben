package com.gxuwz.cashbook.bean;

import java.io.Serializable;

/**
 * 月收支视图对象
 * Created by h on 2017/5/26.
 */

public class MonthCostVO{
    String name;    // 支出类型
    int value;      // 金额

    public MonthCostVO(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
