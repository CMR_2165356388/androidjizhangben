package com.gxuwz.cashbook.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by h on 2017/5/9.
 */

public class IntentUtils {

    /**
     * 跳转到不同的Activity
     * @param context
     * @param cs
     */
    public static void jump(Context context,Class<?> cs){
        Intent intent = new Intent(context,cs);
        context.startActivity(intent);
    }

    /**
     * 传递数据
     * @param context
     * @param cs
     * @param bundle
     */
    public static void passData(Context context,Class<?> cs,Bundle bundle){
        Intent intent = new Intent(context,cs);
        intent.putExtras(bundle); //把该对象传到另一个页面
        context.startActivity(intent);
    }

}
