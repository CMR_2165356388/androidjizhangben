package com.gxuwz.cashbook.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by h on 2017/4/12.
 */

public class DateUtils {

    private static Date date = new Date();

    public static String getDateTimeInstance(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d HH:mm:ss");
        return sdf.format(date);
    }

    public static String getDateInstance(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d");
        return sdf.format(date);
    }

    public static String getMonthInstance(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("M");
        return sdf.format(date);
    }

    public static String getMonthAndDateInstance(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("M月d日");
        return sdf.format(date);
    }

    public static String getCurrentYearAndMonthInstance(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("y-M");
        return sdf.format(date);
    }

    public static String getTimeInstance(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(date);
    }

    public static String getCurrentDate(){
        return getDateInstance(date);
    }

    public static String getCurrentMonth(){
        return getMonthInstance(date);
    }

    public static String getCurrentMonthAndDate(){
        return getMonthAndDateInstance(date);
    }

    public static String getCurrentYearAndMonth(){ return getCurrentYearAndMonthInstance(date); }

}
