package com.gxuwz.cashbook.util;

import java.text.DecimalFormat;

/**
 * Created by h on 2017/5/25.
 */

public class DoubleUtils {
    /**
     * 保留两位小数
     * @param d
     * @return
     */
    public static String formatDouble(double d){
        return new DecimalFormat("#.00").format(d);
    }
}
