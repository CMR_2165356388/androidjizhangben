package com.gxuwz.cashbook.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by h on 2017/5/25.
 */

public class ToastUtils {

    public static void toastLong(Context context,String msg){
        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
    }

    public static void toastShort(Context context,String msg){
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }


}
